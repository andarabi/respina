FROM python:3.7

ARG app_setting
ARG db_username
ARG db_username
ARG db_password
ARG db_server
ARG db_name
ARG db_engin

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .

ENV APP_SETTINGS=$app_setting
ENV DATABASE_URL="$db_engin://$db_username:$db_password@$db_server/$db_name"
CMD ["python","manage.py", "runserver","-h","0.0.0.0","-p","5000"]
